﻿using System.Xml.Serialization;

namespace XmlConverter
{
    /// <summary>
    /// Structre of Xml for convertion
    /// </summary>
    public class C2MVerifyRequest
    {
        [XmlRoot(ElementName = "param")]
        public class Param
        {
            [XmlElement(ElementName = "name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "param_list")]
        public class ParamList
        {
            [XmlElement(ElementName = "param")]
            public Param Param { get; set; }
        }

        [XmlRoot(ElementName = "c2m_verify")]
        public class C2mVerify
        {
            [XmlElement(ElementName = "oper_local_id")]
            public string OperLocalId { get; set; }
            [XmlElement(ElementName = "sale_point_id")]
            public string SalePointId { get; set; }
            [XmlElement(ElementName = "param_list")]
            public ParamList ParamList { get; set; }
        }
    }
}
