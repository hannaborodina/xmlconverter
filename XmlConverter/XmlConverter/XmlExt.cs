﻿
using System.Xml;

namespace XmlConverter
{
    /// <summary>
    /// Extention class for XmlDocument
    /// </summary>
    public static class XmlExt
    {
        /// <summary>
        /// Method for XmlDocument transformation 
        /// </summary>
        /// <param name="document">XmlDocument for transformation</param>
        /// <returns></returns>
        public static XmlDocument GenerateXML(this XmlDocument document)
        {
            var docWithoutDeclaration = document.LastChild;
            var paramList = docWithoutDeclaration.SelectSingleNode("param_list");

            if (paramList == null)
            {
                return document;
            }

            var @params = paramList.SelectNodes("param");

            foreach (XmlNode node in @params)
            {
                TransformNode(node, "name", document);
                TransformNode(node, "value", document);
            }

            return document;
        }

        /// <summary>
        /// Transform childnodes to attributes
        /// </summary>
        /// <param name="node">Node for transformation</param>
        /// <param name="nodeName">Name of childnode for transformation</param>
        /// <param name="document">Document for transformation</param>
        private static void TransformNode(XmlNode node, string nodeName, XmlDocument document)
        {
            var selectedNode = node.SelectSingleNode(nodeName);
            var selectedNodeText = selectedNode.InnerText;

            var attribute = document.CreateAttribute(nodeName);
            attribute.Value = selectedNodeText;

            node.Attributes.Append(attribute);

            node.RemoveChild(selectedNode);
        }
    }
}
