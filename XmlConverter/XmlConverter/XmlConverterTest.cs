﻿using FluentAssertions;
using System.IO;
using System.Xml;
using Xunit;

namespace XmlConverter
{

    /// <summary>
    /// Test for XmlExt
    /// </summary>
    public class XmlConverterTest
    {
        [Fact(DisplayName = "test converting1")]
        public void ConvertXml()
        {
            var xmlstring = File.ReadAllText("./Data/verify.xml");
            var document = new XmlDocument();
            document.LoadXml(xmlstring);
            var newXml = document.GenerateXML();

            newXml.Should().NotBeNull();
        }

        [Fact(DisplayName = "test converting2")]
        public void ConvertXmlWithEmptyParams()
        {
            var xmlstring = File.ReadAllText("./Data/verifyWithEmptyParams.xml");
            var document = new XmlDocument();
            document.LoadXml(xmlstring);
            var newXml = document.GenerateXML();

            newXml.Should().NotBeNull();
        }

        [Fact(DisplayName = "test converting3")]
        public void ConvertXmlWithNoParams()
        {
            var xmlstring = File.ReadAllText("./Data/verifyWithoutParams.xml");
            var document = new XmlDocument();
            document.LoadXml(xmlstring);
            var newXml = document.GenerateXML();

            newXml.Should().NotBeNull();
        }
    }
}
